import React from 'react';
import {
  Text, Image, TouchableOpacity, ImageBackground,
} from 'react-native';
import styles from './styles';
import logo from '../../../assets/logo.png';
import bgImage from '../../../assets/bg2.jpg';

const Main = (props) => {
  const { navigation } = props;
  return (
    <ImageBackground source={bgImage} style={styles.container}>
      <Image style={styles.image} source={logo} />
      <Text style={styles.title}>Toodler</Text>
      <Text style={styles.paragraph}>The new and easy way to keep track of your ideas!</Text>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('Boards');
        }}
        style={styles.button}
      >
        <Text
          style={styles.buttonText}
        >
          Get Started

        </Text>
      </TouchableOpacity>
    </ImageBackground>
  );
};

export default Main;
