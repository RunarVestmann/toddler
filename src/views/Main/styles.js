import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  image: {
    marginTop: 20,
    marginBottom: -50,
    width: 150,
    height: 250,
  },
  title: {
    marginTop: -50,
    marginBottom: -50,
    fontSize: 56,
    fontWeight: 'bold',
    color: 'rgba(50,50,50, 1.0)',
  },
  paragraph: {
    fontSize: 20,
    color: 'rgba(30,30,30, 1.0)',
    textAlign: 'center',
    paddingRight: 30,
    paddingLeft: 30,
  },
  button: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
    borderColor: 'white',
    borderWidth: 2,
    backgroundColor: 'rgba(70,70,70, .7)',
    borderRadius: 2,
  },
  buttonText: {
    fontWeight: 'bold',
    color: 'white',
  },
  background: {
    width: '100%',
    height: '100%',
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
});
