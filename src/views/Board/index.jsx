import React from 'react';
import {
  View,
  Text,

  FlatList,
  ImageBackground,
} from 'react-native';

import BoardThumbnail from '../../components/BoardThumbnail';
import List from '../../components/List';
import listService from '../../services/listService';
import taskService from '../../services/taskService';
import boardService from '../../services/boardService';
import Toolbar from '../../components/Toolbar';
import ListModal from '../../components/ListModal';
import bgImage from '../../../assets/bg2.jpg';

import styles from './styles';

const text = 'Board Description:\n';

class Board extends React.Component {
  constructor(props) {
    super(props);

    const { navigation } = props;
    this.popupRef = React.createRef();
    const id = navigation.getParam('boardId', '');
    this.state = {
      lists: listService.getListsByBoardId(id),
      board: boardService.getBoardById(id),
      isListModalOpen: false,
      isEditingList: false,
      refresh: false,
      selectedLists: [],
      activeList: {},
    };
  }

  handleListSubmit(item) {
    const {

      refresh,
      lists,
      board,
    } = this.state;

    if (item.name !== '' && item.name !== undefined) {
      if (item.id > 0) {
        const list = lists.find((l) => l.id === item.id);
        list.name = item.name;
        list.color = item.color;
      } else {
        listService.addNewList({
          name: item.name,
          color: item.color,
          boardId: board.id,
        });
      }

      this.setState({
        isListModalOpen: false,
        lists: listService.getListsByBoardId(board.id),
        refresh: !refresh,
      });
    } else {
      let errorMessage = 'Error occured, could not create a new list';
      if (!item.name) {
        errorMessage = 'Your new list needs to have a name';
      }

      // eslint-disable-next-line no-alert
      alert(errorMessage);
    }
  }

  handleListLongPress(id) {
    const { selectedLists } = this.state;
    if (selectedLists.indexOf(id) === -1) {
      this.setState({ selectedLists: [...selectedLists, id] });
    } else {
      this.setState({
        selectedLists: selectedLists.filter((listId) => listId !== id),
      });
    }
  }

  deleteSelectedLists() {
    const { selectedLists, board, refresh } = this.state;
    const allLists = listService.getAllLists();
    const allTasks = taskService.getAllTasks();
    selectedLists.forEach((listId) => {
      const index = allLists.indexOf(listService.getListById(listId));
      if (index !== -1) {
        allLists.splice(index, 1);
      }
      const tasksOnList = taskService.getTasksByListId(listId);
      tasksOnList.forEach((task) => {
        const taskIndex = allTasks.indexOf(task);
        if (taskIndex !== -1) allTasks.splice(taskIndex, 1);
      });
    });

    this.setState({
      selectedLists: [],
      lists: listService.getListsByBoardId(board.id),
      refresh: !refresh,
    });
  }

  displaySelectedListCount() {
    const { selectedLists } = this.state;
    const caption = selectedLists.length === 1 ? 'list' : 'lists';
    if (selectedLists.length === 0) {
      return (
        <Text style={styles.caption}>
          No
          {' '}
          {caption}
          {' '}
          selected
        </Text>
      );
    }
    return (
      <Text style={styles.caption}>
        {selectedLists.length}
        {' '}
        {caption}
        {' '}
        selected
      </Text>
    );
  }

  render() {
    const {
      isListModalOpen,
      isEditingList,
      board,
      selectedLists,
      activeList,
      lists,
      refresh,
    } = this.state;
    const { navigation } = this.props;
    return (
      <ImageBackground source={bgImage} style={{ flex: 1 }}>
        <Toolbar
          onAdd={() => this.setState({
            isListModalOpen: true,
            isEditingList: false,
            activeList: { },
          })}
          onDelete={() => this.deleteSelectedLists()}
          isSomethingSelected={selectedLists.length > 0}
        />
        <View style={{ marginTop: -20, alignSelf: 'center' }}>
          {this.displaySelectedListCount()}
        </View>
        <View style={styles.container}>
          <BoardThumbnail
            id={board.id}
            name={board.name}
            thumbnailPhoto={board.thumbnailPhoto}
          />
        </View>
        <View style={styles.margins}>
          {board.description ? (
            <Text style={styles.boardDescription}>
              {text}
              <Text style={styles.boardDescription}>{board.description}</Text>
            </Text>
          ) : (
            <></>
          )}
        </View>
        <ListModal
          isOpen={isListModalOpen}
          list={activeList}
          title={isEditingList ? 'Edit list' : 'Create list'}
          closeModal={() => this.setState({
            isListModalOpen: false,
            activeList: {},
          })}
          onSubmit={(item) => this.handleListSubmit(item)}
          isEdit={isEditingList}
        />
        <View style={{ height: '70%' }}>
          <FlatList
            numColumns={1}
            data={lists}
            extraData={(lists, refresh, selectedLists)}
            renderItem={({ item: { id, name, color } }) => (
              <List
                isSelected={selectedLists.indexOf(id) !== -1}
                id={id}
                onLongPress={(listId) => this.handleListLongPress(listId)}
                onPress={(listId) => {
                  this.setState({ selectedLists: [] });
                  navigation.navigate('Tasks', { listId });
                }}
                onEditPressed={() => this.setState({
                  isListModalOpen: true,
                  isEditingList: true,
                  activeList: { id, name, color },
                })}
                color={color}
                name={name}
              />
            )}
            keyExtractor={(item) => item.id.toString()}
          />
        </View>
      </ImageBackground>
    );
  }
}

export default Board;
