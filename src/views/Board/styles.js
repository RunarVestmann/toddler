import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'rgba(255,255,255, .7)',
    height: 75,
  },
  cardContainer: {
    paddingTop: 5,
  },
  displayedLists: {
    marginTop: -20,
    alignSelf: 'center',
  },
  boardDescription: {
    fontSize: 16,
    color: 'rgba(30,30,30, 1.0)',
    textAlign: 'center',
    paddingRight: 30,
    paddingLeft: 30,
  },
  caption: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  margins: {
    paddingTop: 10,
    marginBottom: 10,
  },
});
