import React from 'react';
import {
  FlatList,
  View,
  Text,
  ImageBackground,
} from 'react-native';

import Toolbar from '../../components/Toolbar';
import BoardThumbnail from '../../components/BoardThumbnail';
import boardService from '../../services/boardService';
import listService from '../../services/listService';
import taskService from '../../services/taskService';
import AddModal from '../../components/BoardModal';
import styles from './styles';
import bgImage from '../../../assets/bg2.jpg';

class Boards extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedBoards: [],
      boards: boardService.getAllBoards(),
      isAddModalOpen: false,
      editingBoard: false,
      refresh: false,
      activeBoard: {},
    };
  }

  handleSubmit(item) {
    const { boards, refresh } = this.state;
    // id, name, thumbnailPhoto, description
    if (
      item.name !== undefined
      && item.name !== ''
      && item.thumbnailPhoto !== undefined
      && item.thumbnailPhoto !== ''
    ) {
      if (item.id > 0) {
        const board = boards.find((b) => b.id === item.id);
        board.name = item.name;
        board.thumbnailPhoto = item.thumbnailPhoto;
        board.description = item.description;
      } else {
        boardService.addNewBoard(item);
      }

      this.setState({
        isAddModalOpen: false,
        refresh: !refresh,
      });
    } else {
      let errorMessage = 'Error occured, could not create a new board';
      if (!item.name) {
        errorMessage = 'Your new board needs to have a name';
      } else if (!item.thumbnailPhoto) {
        errorMessage = 'Your new board needs to have a thumbnail photo';
      }
      // eslint-disable-next-line no-alert
      alert(errorMessage);
    }
  }

  onBoardLongPress(id) {
    const { selectedBoards } = this.state;
    if (selectedBoards.indexOf(id) === -1) {
      this.setState({ selectedBoards: [...selectedBoards, id] });
    } else {
      this.setState({
        selectedBoards: selectedBoards.filter((boardId) => boardId !== id),
      });
    }
  }

  deleteSelectedBoards() {
    const { selectedBoards, boards } = this.state;
    selectedBoards.forEach((boardId) => {
      const board = boardService.getBoardById(boardId);
      const index = boards.indexOf(board);
      if (index !== -1) {
        // Delete board
        boards.splice(index, 1);

        // Delete lists and tasks
        const allLists = listService.getAllLists();
        const allTasks = taskService.getAllTasks();

        const listsOnBoard = listService.getListsByBoardId(board.id);

        listsOnBoard.forEach((list) => {
          const listIndex = allLists.indexOf(list);
          if (listIndex !== -1) allLists.splice(listIndex, 1);
          const tasksOnList = taskService.getTasksByListId(list.id);
          tasksOnList.forEach((task) => {
            const taskIndex = allTasks.indexOf(task);
            if (taskIndex !== -1) allTasks.splice(taskIndex, 1);
          });
        });
      }
    });

    this.setState({
      selectedBoards: [],
      boards,
    });
  }

  displaySelectedBoardCount() {
    const { selectedBoards } = this.state;
    const caption = selectedBoards.length === 1 ? 'board' : 'boards';
    if (selectedBoards.length === 0) {
      return (
        <Text style={styles.caption}>
          No
          {' '}
          {caption}
          {' '}
          selected
        </Text>
      );
    }
    return (
      <Text style={styles.caption}>
        {selectedBoards.length}
        {' '}
        {caption}
        {' '}
        selected
      </Text>
    );
  }

  render() {
    const { navigation } = this.props;
    const {
      selectedBoards,
      isAddModalOpen,
      boards,
      editingBoard,
      refresh,
      activeBoard,
    } = this.state;
    return (
      <ImageBackground source={bgImage} style={styles.bgImage}>
        <Toolbar
          onAdd={() => this.setState({
            isAddModalOpen: true,
            activeBoard: {},
          })}
          onDelete={() => this.deleteSelectedBoards()}
          isSomethingSelected={selectedBoards.length > 0}
        />
        <View style={{ marginTop: -20, alignSelf: 'center' }}>
          {this.displaySelectedBoardCount()}
        </View>
        <AddModal
          title={editingBoard ? 'Edit board' : 'Create board'}
          activeBoard={activeBoard}
          isOpen={isAddModalOpen}
          closeModal={() => this.setState({ isAddModalOpen: false, editingBoard: false })}
          onSubmit={(item) => this.handleSubmit(item)}
        />
        <FlatList
          data={boards}
          extraData={(selectedBoards, boards, refresh)}
          renderItem={({
            item: {
              id, name, thumbnailPhoto, description,
            },
          }) => (
            <BoardThumbnail
              onPress={(boardId) => {
                this.setState({ selectedBoards: [] });
                navigation.navigate('Board', { boardId });
              }}
              onPenPressed={() => this.setState({
                isAddModalOpen: true,
                editingBoard: true,
                activeBoard: {
                  id,
                  name,
                  thumbnailPhoto,
                  description,
                },
              })}
              onLongPress={(boardId) => this.onBoardLongPress(boardId)}
              id={id}
              name={name}
              description={description}
              thumbnailPhoto={thumbnailPhoto}
              isSelected={selectedBoards.indexOf(id) !== -1}
            />
          )}
          keyExtractor={(item) => item.id.toString()}
        />
      </ImageBackground>
    );
  }
}

export default Boards;
