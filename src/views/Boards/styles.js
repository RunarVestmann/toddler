import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  caption: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  bgImage: {
    width: '100%',
    height: '100%',
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  displayedBoards: {
    marginTop: -20,
    alignSelf: 'center',
  },
});
