import React from 'react';
import { View, Text, ImageBackground } from 'react-native';
import TaskModal from '../../components/TaskModal';
import Toolbar from '../../components/Toolbar';
import TaskList from '../../components/TaskList';
import taskService from '../../services/taskService';
import styles from './styles';
import bgImage from '../../../assets/bg2.jpg';
import listService from '../../services/listService';
import List from '../../components/List';

class Tasks extends React.Component {
  constructor(props) {
    super(props);

    const { navigation } = props;
    const id = navigation.getParam('listId', '');
    this.state = {
      tasks: taskService.getTasksByListId(id),
      currentIndex: -1,
      refresh: false,
      isTaskModalOpen: false,
      isEditingTask: false,
      listId: id,
      selectedTasks: [],
      activeTask: {},
      unfinishedTasks: taskService.getUnfinishedTasks(id),
    };
  }

  handleSubmit(item) {
    const { listId, tasks, refresh } = this.state;

    if (item.name !== '' && item.name !== undefined) {
      if (item.id > 0) {
        const task = tasks.find((t) => t.id === item.id);
        task.name = item.name;
        task.description = item.description;
        task.listId = item.listid;
      } else {
        taskService.addNewTask({
          name: item.name,
          description: item.description,
          isFinished: false,
          listId: item.listid,
        });
      }

      this.setState({
        isTaskModalOpen: false,
        tasks: taskService.getTasksByListId(listId),
        refresh: !refresh,
        activeTask: {},
        unfinishedTasks: taskService.getUnfinishedTasks(listId),
      });
    } else {
      let errorMessage = 'Error occured, could not create a new board';
      if (!item.name) {
        errorMessage = 'Your task needs to have a name';
      }

      // eslint-disable-next-line no-alert
      alert(errorMessage);
    }
  }

  handleTaskLongPress(id) {
    const { selectedTasks } = this.state;
    if (selectedTasks.indexOf(id) === -1) {
      this.setState({ selectedTasks: [...selectedTasks, id] });
    } else {
      this.setState({
        selectedTasks: selectedTasks.filter((boardId) => boardId !== id),
      });
    }
  }

  setCurrentIndex(id) {
    const { currentIndex, refresh } = this.state;
    if (currentIndex === id) {
      this.setState({ currentIndex: -1 });
    } else {
      this.setState({ currentIndex: id });
    }
    this.setState({ refresh: !refresh });
  }

  setCompleted(id, isFinished) {
    const { tasks, refresh } = this.state;

    const list = tasks.find((l) => l.id === id);
    list.isFinished = isFinished;
    this.setState({
      currentIndex: -1,
      unfinishedTasks: taskService.getUnfinishedTasks(list.listId),
      refresh: !refresh,
    });
  }

  deleteSelectedTasks() {
    const { selectedTasks, listId } = this.state;
    const tasks = taskService.getAllTasks();
    selectedTasks.forEach((boardId) => {
      const board = taskService.getTaskById(boardId);
      const index = tasks.indexOf(board);
      if (index !== -1) {
        tasks.splice(index, 1);
      }
    });

    this.setState({
      selectedTasks: [],
      tasks: taskService.getTasksByListId(listId),
      unfinishedTasks: taskService.getUnfinishedTasks(listId),
    });
  }

  displaySelectedTaskCount() {
    const { selectedTasks } = this.state;
    const caption = selectedTasks.length === 1 ? 'task' : 'tasks';
    if (selectedTasks.length === 0) {
      return (
        <Text style={styles.caption}>
          No
          {' '}
          {caption}
          {' '}
          selected
        </Text>
      );
    }
    return (
      <Text style={styles.caption}>
        {selectedTasks.length}
        {' '}
        {caption}
        {' '}
        selected
      </Text>
    );
  }

  displayUnfinishedTaskCount() {
    const { unfinishedTasks, tasks } = this.state;
    if (tasks.length === 0) {
      return (
        <Text style={styles.caption}>
          This list is empty
        </Text>
      );
    }
    if (unfinishedTasks === 0) {
      return (
        <Text style={styles.caption}>
          All tasks in this list have been finished
        </Text>
      );
    }
    if (unfinishedTasks === 1) {
      return (
        <Text style={styles.caption}>
          There is 1 unfinished task in this list
        </Text>
      );
    }
    return (
      <Text style={styles.caption}>
        There are
        {' '}
        {unfinishedTasks}
        {' '}
        unfinished tasks
        {' '}
        in this list
      </Text>
    );
  }

  render() {
    const {
      tasks,
      currentIndex,
      refresh,
      isTaskModalOpen,
      isEditingTask,
      selectedTasks,
      listId,
      activeTask,
    } = this.state;

    const list = listService.getListById(listId);
    const hex2rgba = (hex) => {
      const [r, g, b] = hex.match(/\w\w/g).map((x) => parseInt(x, 16));
      return `rgba(${r},${g},${b},0.1)`;
    };

    return (
      <ImageBackground source={bgImage} style={styles.container}>
        <TaskModal
          isOpen={isTaskModalOpen}
          closeModal={() => this.setState({ isTaskModalOpen: false })}
          title={isEditingTask ? 'Edit task' : 'Create task'}
          onSubmit={(item) => this.handleSubmit(item)}
          listId={listId}
          activeTask={activeTask}
          boardId={listService.getListById(listId).boardId}
          isEdit={isEditingTask}
        />
        <Toolbar
          onAdd={() => this.setState({
            isTaskModalOpen: true,
            isEditingTask: false,
            activeTask: {},
          })}
          onDelete={() => this.deleteSelectedTasks()}
          isSomethingSelected={selectedTasks.length > 0}
        />

        <View style={{ marginTop: -20, alignSelf: 'center' }}>
          {this.displaySelectedTaskCount()}
        </View>
        <List
          style={styles.list}
          id={list.id}
          color={list.color}
          name={list.name}
          editHidden={true}
        />
        <View style={{ marginTop: 10, marginBottom: 10, alignSelf: 'center' }}>
          {this.displayUnfinishedTaskCount()}
        </View>
        <View style={{ height: '84%', backgroundColor: hex2rgba(list.color) }}>
          <TaskList
            taskList={tasks}
            onPress={(id) => this.setCurrentIndex(id)}
            currentIndex={currentIndex}
            refresh={refresh}
            onComplete={(id, isFinished) => this.setCompleted(id, isFinished)}
            onLongPress={(taskId) => this.handleTaskLongPress(taskId)}
            selectedTasks={selectedTasks}
            onEditPressed={(task) => this.setState({
              isTaskModalOpen: true,
              isEditingTask: true,
              activeTask: task,
            })}
          />
        </View>
      </ImageBackground>
    );
  }
}

export default Tasks;
