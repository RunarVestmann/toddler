import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  cardContainer: {
    paddingTop: 5,
  },
  boardDescription: {
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  list: {
    padding: 50,
  },

});
