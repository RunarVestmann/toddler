import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Boards from '../views/Boards';
import Board from '../views/Board';
import Tasks from '../views/Tasks';
import Home from '../views/Main';

export default createAppContainer(
  createStackNavigator({
    Home: {
      screen: Home,
      navigationOptions: {
        headerShown: false,
      },
    },
    Boards: {
      screen: Boards,
      navigationOptions: {
        headerStyle: {
          height: 75,
        },
        headerTitleStyle: {
          fontSize: 22,
          fontWeight: '900',
          textTransform: 'uppercase',
        },
        title: 'My Boards',
        headerTitleAlign: 'center',
      },
    },
    Board: {
      screen: Board,
      navigationOptions: {
        headerStyle: {
          height: 75,
        },
        headerTitleStyle: {
          fontSize: 22,
          fontWeight: '900',
          textTransform: 'uppercase',
        },
        title: 'Current Board',
        headerTitleAlign: 'center',
      },
    },
    Tasks: {
      screen: Tasks,
      navigationOptions: {
        headerStyle: {
          height: 75,
        },
        headerTitleStyle: {
          fontSize: 22,
          fontWeight: '900',
          textTransform: 'uppercase',
        },
        title: 'Current Tasks',
        headerTitleAlign: 'center',
      },
    },
  }),
);
