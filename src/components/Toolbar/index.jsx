import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { FontAwesome5, Octicons } from '@expo/vector-icons';

import PropTypes from 'prop-types';
import styles from './styles';

const Toolbar = ({
  isSomethingSelected, onAdd, onDelete, size,
}) => (
  <View style={styles.container}>
    <TouchableOpacity style={styles.toolbarItem} onPress={onAdd}>
      <FontAwesome5 name="plus-square" size={size} color="black" />
    </TouchableOpacity>
    <TouchableOpacity
      style={styles.toolbarItem}
      onPress={onDelete}
      disabled={!isSomethingSelected}
    >
      <Octicons
        name="trashcan"
        size={size}
        color="black"
        style={!isSomethingSelected ? { color: 'rgba(155, 155, 155, .5)' } : {}}
      />
    </TouchableOpacity>
  </View>
);

Toolbar.propTypes = {
  isSomethingSelected: PropTypes.bool,
  onAdd: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  size: PropTypes.number,
};

Toolbar.defaultProps = {
  isSomethingSelected: false,
  size: 30,
};

export default Toolbar;
