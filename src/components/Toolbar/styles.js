import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 20,
    marginBottom: 20,
  },
  toolbarItem: {
    paddingRight: 20,
    paddingBottom: 20,
  },
});
