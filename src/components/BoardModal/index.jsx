import React from 'react';
import { Entypo } from '@expo/vector-icons';
import {
  TouchableOpacity,
  Text,
  TextInput,
  View,
  Button,
  ImageBackground,
} from 'react-native';
import PropTypes from 'prop-types';
import Modal from '../Modal';
import styles from './styles';

import { takePhoto, selectFromCameraRoll } from '../../services/imageService';
import { addImage } from '../../services/fileService';

class BoardModal extends React.Component {
  constructor(props) {
    super(props);
    const { name, description } = props;
    this.state = {
      initialNamePlaceholder: name || 'Board name',
      initalDescriptionPlaceholder: description || 'Board description',
      refresh: false,
    };
  }

  async takeCameraPhoto() {
    const photo = await takePhoto();
    if (photo.length > 0) {
      await this.addImage(photo);
    }
  }

  async addImage(image) {
    const newImage = await addImage(image);

    // eslint-disable-next-line no-alert
    if (newImage === null) alert('Image could not be loaded');
    else {
      const { activeBoard } = this.props;
      const { refresh } = this.state;

      activeBoard.thumbnailPhoto = `data:image/jpeg;base64,${newImage.file}`;
      this.setState({ refresh: !refresh });
    }
  }

  async selectImageFromCameraRoll() {
    const photo = await selectFromCameraRoll();
    if (photo.length > 0) {
      await this.addImage(photo);
    }
  }

  render() {
    const {
      isOpen, closeModal, title, onSubmit, activeBoard,
    } = this.props;
    const { initialNamePlaceholder, initalDescriptionPlaceholder } = this.state;
    const submit = () => {
      onSubmit(activeBoard);
    };

    return (
      <Modal isOpen={isOpen} closeModal={closeModal} title={title}>
        <TextInput
          name="title"
          style={styles.inputs}
          placeholder={initialNamePlaceholder}
          defaultValue={activeBoard.name}
          // eslint-disable-next-line no-return-assign
          onChangeText={(text) => (activeBoard.name = text)}
        />
        <TextInput
          style={styles.inputs}
          placeholder={initalDescriptionPlaceholder}
          defaultValue={activeBoard.description}
          // eslint-disable-next-line no-return-assign
          onChangeText={(text) => (activeBoard.description = text)}
        />

        <View styles={styles.iconContainer}>
          <Text style={{ marginTop: 5 }}>Thumbnail photo:</Text>
          <ImageBackground
            source={
              activeBoard.thumbnailPhoto !== undefined
                ? {
                  uri: activeBoard.thumbnailPhoto,
                }
                : {}
            }
            style={{ height: 50, width: 100 }}
          />
          <TouchableOpacity onPress={() => this.takeCameraPhoto()}>
            <Entypo style={styles.icon} name="camera" />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.selectImageFromCameraRoll()}>
            <Entypo style={styles.icon} name="image" />
          </TouchableOpacity>

          <View style={{ marginTop: 5 }}>
            <Button title="Submit" onPress={submit} />
          </View>
        </View>
      </Modal>
    );
  }
}

BoardModal.propTypes = {
  name: PropTypes.string,
  description: PropTypes.string,
  isOpen: PropTypes.bool.isRequired,
  closeModal: PropTypes.func,
  // eslint-disable-next-line react/forbid-prop-types
  activeBoard: PropTypes.object,
  title: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

BoardModal.defaultProps = {
  activeBoard: {},
  closeModal: () => {},
  description: '',
  name: '',
};

export default BoardModal;
