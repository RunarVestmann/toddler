import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  icon: {
    fontSize: 40,
    marginTop: 20,
    marginBottom: 5,
    marginLeft: 35,
  },
  iconContainer: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  inputs: {
    height: 30,
    width: '80%',
    borderWidth: 0.5,
    borderColor: '#20232a',
    borderRadius: 3,
    paddingLeft: 5,
    paddingRight: 5,
    marginBottom: 5,
  },
  dropdownBox: {
    backgroundColor: '#fafafa',
  },
  dropdownMenu: {
    height: 40,
    width: '80%',
  },
  dropdownListText: {
    marginBottom: 10,
    marginTop: 5,
  },
});
