import React from 'react';
import { Text, TextInput, View, Button } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import PropTypes from 'prop-types';
import Modal from '../Modal';
import listService from '../../services/listService';

import styles from './styles';

class TaskModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      initialNamePlaceholder: 'Task name',
      initalDescriptionPlaceholder: 'Task description',
    };
  }

  render() {
    const {
      isOpen,
      closeModal,
      title,
      onSubmit,
      activeTask,
      listId,
      boardId,
      isEdit,
    } = this.props;
    const { initialNamePlaceholder, initalDescriptionPlaceholder } = this.state;

    const submit = () => {
      if (activeTask.listid === undefined) {
        activeTask.listid = listId;
      }
      onSubmit(activeTask);
    };

    return (
      <Modal isOpen={isOpen} closeModal={closeModal} title={title}>
        <TextInput
          style={styles.inputs}
          placeholder={initialNamePlaceholder}
          defaultValue={activeTask.name}
          // eslint-disable-next-line no-return-assign
          onChangeText={(text) => (activeTask.name = text)}
        />
        <TextInput
          style={styles.inputs}
          placeholder={initalDescriptionPlaceholder}
          defaultValue={activeTask.description}
          // eslint-disable-next-line no-return-assign
          onChangeText={(text) => (activeTask.description = text)}
        />
        {isEdit ? (
          <Text style={styles.dropdownListText}>Select list</Text>
        ) : (
          <></>
        )}

        {isEdit ? (
          <DropDownPicker
            items={listService
              .getListsByBoardId(boardId)
              .map((l) => ({ label: l.name, value: l.id }))}
            defaultValue={listId}
            containerStyle={styles.dropdownMenu}
            style={styles.dropdownBox}
            dropDownStyle={{ elevation: 100, zIndex: 100 }}
            itemStyle={{
              justifyContent: 'flex-start',
              elevation: 100,
              zIndex: 100,
            }}
            // eslint-disable-next-line no-return-assign
            onChangeItem={(item) => (activeTask.listid = item.value)}
          />
        ) : (
          <></>
        )}
        <View style={{ marginTop: 15 }}>
          <Button title="Submit" onPress={submit} />
        </View>
      </Modal>
    );
  }
}

TaskModal.propTypes = {
  name: PropTypes.string,
  isOpen: PropTypes.bool.isRequired,
  closeModal: PropTypes.func,
  activeTask: PropTypes.shape({
    name: PropTypes.string,
    description: PropTypes.string,
    listid: PropTypes.number,
  }).isRequired,
  title: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  listId: PropTypes.number.isRequired,
  boardId: PropTypes.number.isRequired,
  isEdit: PropTypes.bool.isRequired,
  description: PropTypes.string,
};

TaskModal.defaultProps = {
  closeModal: () => {},
  description: '',
  name: '',
};

export default TaskModal;
