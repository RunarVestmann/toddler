import React from 'react';
import { View, FlatList } from 'react-native';
import PropTypes from 'prop-types';

import Task from '../Task';

function TaskList({
  taskList,
  currentIndex,
  onPress,
  onComplete,
  refresh,
  onLongPress,
  selectedTasks,
  onEditPressed,
}) {
  return (
    <View>
      <FlatList
        numColumns={1}
        data={taskList}
        extraData={(currentIndex, refresh)}
        renderItem={({ item: itm }) => (
          <Task
            task={itm}
            onComplete={onComplete}
            onPress={(id) => (onPress ? onPress(id) : {})}
            currentIndex={currentIndex}
            onLongPress={(taskId) => onLongPress(taskId)}
            isSelected={selectedTasks.indexOf(itm.id) !== -1}
            onEditPressed={(task) => onEditPressed(task)}
          />
        )}
        keyExtractor={(itm) => itm.id.toString()}
      />
    </View>
  );
}

TaskList.propTypes = {
  taskList: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string,
      description: PropTypes.string,
      isFinished: PropTypes.bool.isRequired,
      listId: PropTypes.number.isRequired,
    }),
  ).isRequired,
  currentIndex: PropTypes.number.isRequired,
  refresh: PropTypes.bool.isRequired,
  onPress: PropTypes.func.isRequired,
  onComplete: PropTypes.func.isRequired,
  selectedTasks: PropTypes.arrayOf(PropTypes.number).isRequired,
  onLongPress: PropTypes.func.isRequired,
  onEditPressed: PropTypes.func.isRequired,
};

TaskList.defaultProps = {};

export default TaskList;
