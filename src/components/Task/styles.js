import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  heading: {
    fontSize: 18,
    fontWeight: '500',
  },
  card: {
    flexGrow: 1,
    borderWidth: 0.5,
    borderColor: 'lightgrey',
    padding: 5,
    margin: 2,
  },
  subList: {
    padding: 20,
    height: 150,

  },
  checkMark: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    left: 1,
    fontSize: 16,
    flexDirection: 'row',
  },
  completed: {
    paddingLeft: 5,
    fontWeight: '500',
    fontSize: 16,

  },

});
