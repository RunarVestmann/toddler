import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
// eslint-disable-next-line import/no-extraneous-dependencies
import { Fontisto } from '@expo/vector-icons';
import Icon from 'react-native-vector-icons/MaterialIcons';
import styles from './styles';

const Task = ({
  task,
  currentIndex,
  onPress,
  onComplete,
  onLongPress,
  isSelected,
  onEditPressed,
}) => (
  <View style={[styles.card]}>
    <TouchableOpacity
      onPress={() => (onPress ? onPress(task.id) : {})}
      onLongPress={() => onLongPress(task.id)}
    >
      <View style={{ flex: 1 }}>
        <Text
          style={[
            styles.heading,
            task.isFinished
              ? { textDecorationLine: 'line-through', color: '#839496' }
              : { color: 'black' },
            { opacity: isSelected ? 0.2 : 1 },
          ]}
        >
          {task.name}
        </Text>
      </View>
    </TouchableOpacity>
    {task.id === currentIndex && (
      <View style={styles.subList}>
        <Text>{task.description}</Text>
        <Text>{task.isFinished}</Text>

        <View style={styles.checkMark}>
          <Fontisto
            name={task.isFinished ? 'checkbox-active' : 'checkbox-passive'}
            size={24}
            color="black"
            onPress={() => onComplete(task.id, !task.isFinished)}
          />

          <Text
            style={styles.completed}
            onPress={() => onComplete(task.id, !task.isFinished)}
          >
            Completed?
          </Text>
          {onEditPressed ? (
            <Icon
              onPress={() => (onEditPressed ? onEditPressed(task) : {})}
              name="edit"
              size={20}
              color="black"
              style={{ right: 0, position: 'absolute' }}
            />
          ) : (
            <></>
          )}
        </View>
      </View>
    )}
  </View>
);
Task.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  task: PropTypes.object.isRequired,
  currentIndex: PropTypes.number.isRequired,
  onPress: PropTypes.func,
  onComplete: PropTypes.func.isRequired,
  onLongPress: PropTypes.func,
  isSelected: PropTypes.bool.isRequired,
  onEditPressed: PropTypes.func,
};

Task.defaultProps = {
  onEditPressed: () => {},
  onPress: () => {},
  onLongPress: () => {},
};

export default Task;
