import React from 'react';
import {
  TextInput, View, Button, Text,
} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';
import Modal from '../Modal';
import styles from './styles';

const colorList = [
  {
    label: 'No color',
    value: 'rgba(0,0,0,0)',
  },
  {
    label: 'Black',
    value: '#000000',
    icon: () => (
      <Icon name="format-paint" size={22} color="#000000" />
    ),
  },
  {
    label: 'Blue',
    value: '#0000ff',
    icon: () => (
      <Icon name="format-paint" size={22} color="#0000ff" />
    ),
  },
  {
    label: 'Red',
    value: '#ff0000',
    icon: () => (
      <Icon name="format-paint" size={22} color="#ff0000" />
    ),
  },
  {
    label: 'Green',
    value: '#008000',
    icon: () => (
      <Icon name="format-paint" size={22} color="#008000" />
    ),
  },
  {
    label: 'Yellow',
    value: '#ffff00',
    icon: () => (
      <Icon name="format-paint" size={22} color="#ffff00" />
    ),
  },
  {
    label: 'Brown',
    value: '#a52a2a',
    icon: () => (
      <Icon name="format-paint" size={22} color="#a52a2a" />
    ),
  },
  {
    label: 'Purple',
    value: '#9400d3',
    icon: () => (
      <Icon name="format-paint" size={22} color="#9400d3" />
    ),
  },
  {
    label: 'Orange',
    value: '#ffa500',
    icon: () => (
      <Icon name="format-paint" size={22} color="#ffa500" />
    ),
  },
];

class ListModal extends React.Component {
  constructor(props) {
    super(props);
    const { name } = props;
    this.state = {
      initialNamePlaceholder: name || 'List name',
    };
  }

  render() {
    const {
      isOpen, closeModal, title, onSubmit, list, isEdit,
    } = this.props;
    const { initialNamePlaceholder } = this.state;

    const submit = () => {
      if (list.color === undefined) { list.color = defaultColor; }
      onSubmit(list);
    };
    let defaultColor = isEdit ? '#000000' : 'rgba(0,0,0,0)';
    if (list.color !== undefined) { defaultColor = list.color; }
    const colorValues = colorList.find((c) => c.value === defaultColor);
    const defaulColorInfo = {
      label: defaultColor,
      value: defaultColor,
      icon: () => (
        <Icon name="format-paint" size={22} color={defaultColor} />
      ),
    };
    if (colorValues === undefined) {
      colorList.push(defaulColorInfo);
    }

    return (
      <Modal isOpen={isOpen} closeModal={closeModal} title={title}>
        <TextInput
          style={styles.inputs}
          placeholder={initialNamePlaceholder}
          defaultValue={list.name}
          // eslint-disable-next-line no-return-assign
          onChangeText={(text) => (list.name = text)}
        />
        <Text style={styles.dropdownListText}>Select color for the list</Text>
        <DropDownPicker
          items={colorList}
          name="picker"
          containerStyle={styles.dropdownMenu}
          defaultValue={defaultColor}
          style={styles.dropdownBox}
          dropDownStyle={{ elevation: 100, zIndex: 100 }}
          itemStyle={{
            justifyContent: 'flex-start',
            elevation: 100,
            zIndex: 100,
          }}
          // eslint-disable-next-line no-return-assign
          onChangeItem={(item) => (list.color = item.value)}
        />
        <View style={{ marginTop: 15 }}>
          <Button title="Submit" onPress={submit} />
        </View>
      </Modal>
    );
  }
}

ListModal.propTypes = {
  name: PropTypes.string,
  isOpen: PropTypes.bool.isRequired,
  closeModal: PropTypes.func,
  // eslint-disable-next-line react/forbid-prop-types
  list: PropTypes.object,
  title: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  isEdit: PropTypes.bool.isRequired,
};

ListModal.defaultProps = {
  closeModal: () => {},
  list: {},
  name: '',
};

export default ListModal;
