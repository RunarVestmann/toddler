import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
// eslint-disable-next-line import/no-unresolved
import Icon from 'react-native-vector-icons/MaterialIcons';
import styles from './styles';

const ICON_SIZE = 20;

const List = ({
  id,
  name,
  color,
  onLongPress,
  onPress,
  onEditPressed,
  isSelected,
  editHidden,
}) => (
  <TouchableOpacity
    onPress={() => (onPress ? onPress(id) : {})}
    onLongPress={() => (onLongPress ? onLongPress(id) : {})}
    style={[styles.card, { borderTopColor: color }]}
  >
    <View>
      <View style={{ alignItems: 'center' }}>
        <Text style={[styles.heading, { opacity: isSelected ? 0.2 : 1 }]}>
          {name}
        </Text>
      </View>
    </View>
    <View style={styles.more}>
      {onEditPressed && !editHidden ? (
        <Icon
          onPress={() => onEditPressed(id)}
          name="edit"
          size={ICON_SIZE}
          color="black"
          style={{ padding: 10 }}
        />
      ) : (
        <></>
      )}
    </View>
  </TouchableOpacity>
);
List.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  onLongPress: PropTypes.func,
  onPress: PropTypes.func,
  onEditPressed: PropTypes.func,
  color: PropTypes.string,
  isSelected: PropTypes.bool,
  editHidden: PropTypes.bool,
};

List.defaultProps = {
  isSelected: false,
  onEditPressed: undefined,
  onPress: () => false,
  onLongPress: () => false,
  color: '',
  editHidden: false,
};

export default List;
