import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  heading: {
    fontSize: 18,
    fontWeight: '900',
    textTransform: 'uppercase',
    justifyContent: 'center',
  },
  card: {
    flexGrow: 1,
    borderWidth: 0.5,
    borderColor: 'lightgrey',
    borderTopWidth: 5,
    padding: 5,
    margin: 5,
  },
  cardContainer: {
    flexGrow: 1,
    justifyContent: 'center',
    padding: 1,
    margin: 1,
  },
  body: {
    margin: 4,
    fontSize: 18,
    lineHeight: 18 * 1.5,
    textAlign: 'left',
  },
  more: {
    position: 'absolute',
    top: 0,
    right: 1,
    fontSize: 14,
  },
});
