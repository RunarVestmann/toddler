import React from 'react';
import {
  Text, ImageBackground, View, TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';

import styles from './styles';

const BoardThumbnail = ({
  id,
  name,
  description,
  thumbnailPhoto,
  onPress,
  onLongPress,
  isSelected,
  onPenPressed,
}) => (
  <TouchableOpacity
    onPress={() => (onPress ? onPress(id) : {})}
    onLongPress={() => (onLongPress ? onLongPress(id) : {})}
  >
    <ImageBackground source={{ uri: thumbnailPhoto }} style={styles.container}>
      <View
        style={[
          isSelected ? styles.inner2 : styles.inner,
          { flexDirection: 'row' },
        ]}
      >
        <View style={styles.subtitle}>
          <Text style={styles.heading}>{name}</Text>
          {description && <Text style={styles.sidePadding}>{description}</Text>}
        </View>
        {onPenPressed ? (
          <View style={styles.pen}>
            <Icon
              style={{ padding: 10 }}
              onPress={() => (onPenPressed ? onPenPressed() : {})}
              name="edit"
              size={20}
              color="black"
            />
          </View>

        ) : (
          <></>
        )}
      </View>
    </ImageBackground>
  </TouchableOpacity>
);

BoardThumbnail.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  thumbnailPhoto: PropTypes.string.isRequired,
  description: PropTypes.string,
  onPress: PropTypes.func,
  onLongPress: PropTypes.func,
  isSelected: PropTypes.bool,
  onPenPressed: PropTypes.func,
};

BoardThumbnail.defaultProps = {
  onPress: null,
  onLongPress: null,
  onPenPressed: null,
  isSelected: false,
  description: undefined,
};

export default BoardThumbnail;
