import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  sidePadding: {
    paddingRight: 35,
    paddingLeft: 35,
  },
  container: {
    flex: 1,
    height: 75,
  },
  heading: {
    fontSize: 18,
    fontWeight: '900',
    textTransform: 'uppercase',
  },
  subtitle: {
    alignItems: 'center',
  },
  inner: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(255,255,255, .7)',
  },
  inner2: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(70,70,70, .7)',
  },
  pen: {
    position: 'absolute',
    right: 10,
    width: 40,
  },
});
