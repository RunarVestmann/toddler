import React from 'react';
import NativeModal from 'react-native-modal';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const Modal = ({
  isOpen, closeModal, title, children,
}) => (
  <NativeModal
    isVisible={isOpen}
    hasBackdrop
    onBackButtonPress={closeModal}
    onBackdropPress={closeModal}
    style={styles.modal}
  >
    <View style={styles.body}>
      <Text style={styles.title}>{title}</Text>
      {children}
    </View>
  </NativeModal>
);

Modal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  closeModal: PropTypes.func,
  title: PropTypes.string,
  // eslint-disable-next-line react/forbid-prop-types
  children: PropTypes.any,
};

Modal.defaultProps = {
  closeModal: () => {},
  title: '',
  children: undefined,
};

export default Modal;
