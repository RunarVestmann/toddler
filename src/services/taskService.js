import data from './data';

let nextId = data.tasks.length;

function getAllTasks() {
  return data.tasks;
}

function getTasksByListId(id) {
  return data.tasks.filter((task) => task.listId === id);
}

function addNewTask(task) {
  // eslint-disable-next-line no-plusplus
  data.tasks.push({ id: ++nextId, ...task });
}

function getTaskById(id) {
  return data.tasks.find((t) => t.id === id);
}

function getUnfinishedTasks(id) {
  return (data.tasks.filter((t) => t.listId === id && !t.isFinished)).length;
}

export default {
  getAllTasks,
  getTasksByListId,
  addNewTask,
  getTaskById,
  getUnfinishedTasks,
};
