import data from './data';

let nextId = data.boards.length;

function getAllBoards() {
  return data.boards;
}

function getBoardById(id) {
  return data.boards.find((board) => board.id === id);
}

function addNewBoard(board) {
  // eslint-disable-next-line no-plusplus
  data.boards.push({ id: ++nextId, ...board });
}

export default { getAllBoards, getBoardById, addNewBoard };
