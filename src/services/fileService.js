// eslint-disable-next-line import/no-extraneous-dependencies
import * as FileSystem from 'expo-file-system';

const imageDirectory = `${FileSystem.documentDirectory}images`;

// eslint-disable-next-line consistent-return
const onException = (cb, errorHandler) => {
  try {
    return cb();
  } catch (err) {
    if (errorHandler) {
      return errorHandler(err);
    }
  }
};

export const cleanDirectory = async () => {
  await FileSystem.deleteAsync(imageDirectory);
};

// eslint-disable-next-line no-return-await
export const copyFile = async (file, newLocation) => await onException(
  () => FileSystem.copyAsync({
    from: file,
    to: newLocation,
  }),
  // eslint-disable-next-line no-unused-vars
  (err) => -1,
);

// eslint-disable-next-line no-return-await
export const loadImage = async (fileName) => await onException(() => FileSystem.readAsStringAsync(`${imageDirectory}/${fileName}`, {
  encoding: FileSystem.EncodingType.Base64,
}));

export const addImage = async (imageLocation) => {
  await FileSystem.makeDirectoryAsync(imageDirectory, { intermediates: true });
  const folderSplit = imageLocation.split('/');
  const fileName = folderSplit[folderSplit.length - 1];
  // eslint-disable-next-line consistent-return
  await onException(() => {
    const val = copyFile(imageLocation, `${imageDirectory}/${fileName}`);
    if (val === -1) return null;
  });

  return {
    name: fileName,
    type: 'image',
    file: await loadImage(fileName),
  };
};

// eslint-disable-next-line no-return-await
export const remove = async (name) => await onException(() => FileSystem.deleteAsync(`${imageDirectory}/${name}`, { idempotent: true }));

const setupDirectory = async () => {
  const dir = await FileSystem.getInfoAsync(imageDirectory);
  if (!dir.exists) {
    await FileSystem.makeDirectoryAsync(imageDirectory);
  }
};

export const getAllImages = async () => {
  // Check if directory exists
  await setupDirectory();

  const result = await onException(() => FileSystem.readDirectoryAsync(imageDirectory));
  return Promise.all(
    result.map(async (fileName) => ({
      name: fileName,
      type: 'image',
      file: await loadImage(fileName),
    })),
  );
};
