import data from './data';

let nextId = data.lists.length;

function getAllLists() {
  return data.lists;
}

function getListsByBoardId(id) {
  return data.lists.filter((list) => list.boardId === id);
}

function addNewList(list) {
  // eslint-disable-next-line no-plusplus
  data.lists.push({ id: ++nextId, ...list });
}

function getListById(id) {
  return data.lists.find((l) => l.id === id);
}

export default {
  getAllLists, getListsByBoardId, addNewList, getListById,
};
