**A todo list app written in React Native(Expo)**

Home screen

![Home screen](screenshots/0home_screen.png)

Boards screen

![Boards screen](screenshots/1&6&11boards_screen.png)

Creating a board

![Creating a board](screenshots/2boards_screen_create_board.png)

Filling out new board information

![Filling out new board information](screenshots/3boards_screen_create_board_filled_out.png)

New board created

![New board created](screenshots/4boards_screen_new_board.png)

Board selected

![Board selected](screenshots/5boards_screen_new_board_selected.png)

Board deleted

![Board deleted](screenshots/1&6&11boards_screen.png)

Board details screen

![Board details screen](screenshots/7&10board_details_screen.png)

Tasks screen

![Tasks screen](screenshots/8tasks_screen.png)

Tasks screen

![Tasks screen](screenshots/9tasks_screen1.png)

Board details screen

![Board details screen](screenshots/7&10board_details_screen.png)

Boards screen

![Boards screen](screenshots/1&6&11boards_screen.png)

Board details screen

![Board details screen](screenshots/12board_details_screen1.png)

Tasks screen

![Tasks screen](screenshots/13tasks_screen_2.png)

Tasks screen

![Tasks screen](screenshots/14tasks_screen3.png)
